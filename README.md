# JiraLogger by lolcio

## About 
This project is a application for managment time of log work in Jira

## How to run?
npm install electron -g 
npm install 
npm start

## How to run development with live building?
Start in one terminal: 
  npm start
  
and in second: 
  npm run build.ng.watch

## Technology stack
Angular 4 + Electron + Typescript + Materialize + Sass + Gulp + Angular CLI


## Configuration
Application is looking for configuration file in the same directory: jira-config.json.
This configuration should look like this:

`
{
  "url": "https://jira.pl",
  "username": "myUser@mail.pl",
  "password": "password",
  "projects": [
    "P1",
    "PROJECT_MAGIC"
  ],
  "minLogRatio": 70,
  "visibility": "SomeVidibility",
  "removeFromComment": "(#.*#)",
  "teamMembers": [
    "janek@mail.pl",
    "tadek@mail.pl"
  ]
}
`

# Bundle problem
For run bundle you have to run this command in main directory (in bash/cygnwin):
 1. For linux
  ```
  npm run bundle.linux
  ```
2. For windows
  ```
  npm run bundle.windows
  ```  
3. Next:
```
npm run bundle.linux && 
cp ./src/logo.svg ./bundles/jira-logger-linux-x64/resources/ && 
cd ./bundles/jira-logger-linux-x64/resources/ &&
cp ./app/electron-app.js ./app/index.js &&
cp ./app/index.html . && 
cp ./app/*.js . && 
../jira-logger &&
cd ../../../
```

in windows:
```
npm run bundle.windows && 
cp ./src/logo.svg ./bundles/jira-logger-win32-x64/resources/ && 
cd ./bundles/jira-logger-win32-x64/resources/ &&
cp ./app/electron-app.js ./app/index.js &&
cp ./app/index.html . && 
cp ./app/*.js . && 
cd ../../../
```

