// /* tslint:disable:no-unused-variable */
//
// import {async, TestBed} from '@angular/core/testing';
// import {AppComponent} from './app.component';
// import {MaterializeModule} from 'angular2-materialize';
// import {BrowserModule} from '@angular/platform-browser';
// import {FormsModule} from '@angular/forms';
// import {HttpModule} from '@angular/http';
// import {DragulaModule} from 'ng2-dragula/ng2-dragula';
// import {JiraConnector} from './service/JiraConnector';
// import {ApplicationConfig} from './service/ApplicationConfig';
// import {SettingsComponent} from './component/settings/settings.component';
// import {HistoryComponent} from './component/history/history.component';
// import {ActionsComponent} from './component/actions/actions.component';
// import {RefreshHistoryObserver} from './service/RefreshHistoryObserver';
// import {MinutesToTime} from './component/pipe/minutes-to-time.pipe';
// import {IssuesComponent} from './component/issues/issues.component';
// import {AddLogWorkObserver} from './service/AddLogWorkObserver';
// import {LoaderComponent} from './component/loader/loader.component';
//
// describe('AppComponent', () => {
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         AppComponent,
//         SettingsComponent,
//         HistoryComponent,
//         ActionsComponent,
//         MinutesToTime,
//         IssuesComponent,
//         LoaderComponent
//       ],
//       imports: [
//         BrowserModule,
//         FormsModule,
//         HttpModule,
//         MaterializeModule,
//         DragulaModule
//       ],
//       providers: [
//         JiraConnector,
//         ApplicationConfig,
//         RefreshHistoryObserver,
//         AddLogWorkObserver
//       ],
//     });
//     TestBed.compileComponents();
//   });
//
//   it('should create the app', async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     const app = fixture.debugElement.componentInstance;
//     expect(app).toBeTruthy();
//   }));
//
//   it(`should have as title 'app works!'`, async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     const app = fixture.debugElement.componentInstance;
//     expect(app.title).toEqual('app works!');
//   }));
//
//   it('should render title in a h1 tag', async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     fixture.detectChanges();
//     const compiled = fixture.debugElement.nativeElement;
//     expect(compiled.querySelector('h1').textContent).toContain('app works!');
//   }));
// });
