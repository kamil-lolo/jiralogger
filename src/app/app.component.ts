import {Component, OnInit, EventEmitter} from '@angular/core';
import {ApplicationConfig} from './service/ApplicationConfig';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  modalActions = new EventEmitter<any>();
  firstRun = false;

  constructor(private applicationConfig: ApplicationConfig) {
    if (!applicationConfig.isConfigCorrect()) {
      this.firstRun = true;
    } else {
      applicationConfig.clearPasswordIfNotRemember();
    }
  }

  showAbout(): void {
    this.modalActions.emit({action: 'modal', params: ['open']});
  }

  closeAbout(): void {
    this.modalActions.emit({action: 'modal', params: ['close']});
  }

  ngOnInit(): void {
    console.log('Welcome in Log me! If you want help please check: http://github.com/klolo');
  }
}
