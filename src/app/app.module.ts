import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {MaterializeModule} from 'angular2-materialize';
import {DragulaModule} from 'ng2-dragula/ng2-dragula';
import {JiraConnector} from './service/JiraConnector';
import {ApplicationConfig} from './service/ApplicationConfig';
import {SettingsComponent} from './component/settings/settings.component';
import {HistoryComponent} from './component/history/history.component';
import {ActionsComponent} from './component/actions/actions.component';
import {RefreshHistoryObserver} from './service/RefreshHistoryObserver';
import {MinutesToTime} from './component/pipe/minutes-to-time.pipe';
import {IssuesComponent} from './component/issues/issues.component';
import {AddLogWorkObserver} from './service/AddLogWorkObserver';
import {LoaderComponent} from './component/loader/loader.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {FullscreenMessageComponent} from './component/fullscreen-message/fullscreen-message.component';
import {FavoritesComponent} from './component/favorites/favorites.component';
import {OnboardingComponent} from './component/onboarding/onboarding.component';

@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    HistoryComponent,
    ActionsComponent,
    MinutesToTime,
    IssuesComponent,
    LoaderComponent,
    FullscreenMessageComponent,
    FavoritesComponent,
    OnboardingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterializeModule,
    BrowserAnimationsModule,
    DragulaModule,
    MaterialModule
  ],
  providers: [
    JiraConnector,
    ApplicationConfig,
    RefreshHistoryObserver,
    AddLogWorkObserver
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
