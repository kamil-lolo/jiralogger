import {Component, OnInit, EventEmitter} from '@angular/core';
import {RefreshHistoryObserver} from '../../service/RefreshHistoryObserver';
import {JiraConnector} from '../../service/JiraConnector';
import {AddLogWorkObserver} from '../../service/AddLogWorkObserver';
import {ApplicationConfig} from '../../service/ApplicationConfig';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {
  modalActions = new EventEmitter<any>();
  logWorkDateParams: any;
  issues: Array<any>;
  selectedIssue = '';
  onlyMyIssue = true;
  workTime: string;
  comment: string;
  isFormValid = false;
  customIssueName: string;
  estimated = true;
  logWorkDate: string;

  constructor(private refreshHistoryObserver: RefreshHistoryObserver,
              private jiraConnector: JiraConnector,
              private  applicationConfig: ApplicationConfig,
              private addLogWorkObserver: AddLogWorkObserver) {
    this.logWorkDateParams = {
      format: 'yyyy-mm-dd'
    };

    addLogWorkObserver.subject.subscribe(event => {
      if (!event) {
        return;
      }
      console.log('Add log work event:' + event);
      this.onlyMyIssue = false;
      this.initComponent();
      this.getMyIssues();
      this.selectedIssue = this.getIssueWithoutProjectPrefix(event.issue);
      this.workTime = event.time;
      this.modalActions.emit({action: 'modal', params: ['open']});
      this.getIssueTitle();
    });
  }

  private initComponent(): void {
    this.logWorkDate = new Date().toISOString().split('T')[0];
    this.comment = '';
    this.workTime = '';
    this.estimated = true;
  }

  private getIssueWithoutProjectPrefix(issue: string): string {
    if (issue.startsWith(this.applicationConfig.project)) {
      return issue.substr(this.applicationConfig.project.length + 1);
    }
    return issue;
  }

  getIssueTitle(): void {
    this.customIssueName = '';

    if (!this.selectedIssue) {
      return;
    }

    this.jiraConnector.getIssueDetails(this.applicationConfig.project + '-' + this.getIssueWithoutProjectPrefix(this.selectedIssue))
      .subscribe(issue => {
        this.estimated = issue.fields.timeoriginalestimate > 0;
        this.customIssueName = issue.fields.summary;
      });
  }

  clearCustomIssueName(): void {
    this.customIssueName = '';
    this.selectedIssue = this.getIssueWithoutProjectPrefix(this.selectedIssue);
  }

  refreshHistory(): void {
    this.refreshHistoryObserver.refresh();
  }

  addComment(): void {
    this.initComponent();
    this.getMyIssues();
    this.modalActions.emit({action: 'modal', params: ['open']});
  }

  validateForm(): void {
    console.log('issue selected');
    this.getIssueTitle();
    this.checkField();
  }

  checkField(): void {
    this.isFormValid = !!(this.workTime && this.comment && this.selectedIssue);
  }

  getMyIssues(): void {
    this.jiraConnector.getAssignedIssues().subscribe(
      result => {
        this.issues = result.issues;
      }
    );
  }

  logWork(): void {
    console.log(`Log in ${this.selectedIssue}`);
    if (!this.workTime) {
      alert('lpease correctly fill input');
    }

    let issue = this.selectedIssue;
    if (!this.selectedIssue.startsWith(this.applicationConfig.project)) {
      issue = `${this.applicationConfig.project}-${this.selectedIssue}`;
    }
    this.jiraConnector.addLogWork(issue, this.workTime, this.comment, new Date(this.logWorkDate));
    this.clearCustomIssueName();
    this.closeCommentModal();
  }

  closeCommentModal(): void {
    this.modalActions.emit({action: 'modal', params: ['close']});
    this.clearCustomIssueName();
  }

  ngOnInit(): void {
  }
}
