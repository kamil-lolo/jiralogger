import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-message',
  template: `
    <div style="position: relative; z-index: 1000">
      <div class="messageContent" *ngIf="visible"><p style="margin-top: 35%">{{message}}</p></div>
    </div>  
  `,
  styleUrls: ['./fullscreen-message.component.css']
})
export class FullscreenMessageComponent {
  @Input() message: string;

  @Input() visible: boolean;
}
