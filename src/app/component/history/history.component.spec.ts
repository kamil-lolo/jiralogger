import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MaterializeModule} from 'angular2-materialize';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {DragulaModule} from 'ng2-dragula/ng2-dragula';
import {SettingsComponent} from '../settings/settings.component';
import {HistoryComponent} from '..//history/history.component';
import {ActionsComponent} from '../actions/actions.component';
import {RefreshHistoryObserver} from '../../service/RefreshHistoryObserver';
import {MinutesToTime} from '..//pipe/minutes-to-time.pipe';
import {IssuesComponent} from '../issues/issues.component';
import {AddLogWorkObserver} from '../../service/AddLogWorkObserver';
import {LoaderComponent} from '../loader/loader.component';
import {AppComponent} from '../../app.component';
import {JiraConnector} from '../../service/JiraConnector';
import {ApplicationConfig} from '../../service/ApplicationConfig';

describe('HistoryComponent', () => {
  let component: HistoryComponent;
  let fixture: ComponentFixture<HistoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        SettingsComponent,
        HistoryComponent,
        ActionsComponent,
        MinutesToTime,
        IssuesComponent,
        LoaderComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterializeModule,
        DragulaModule
      ],
      providers: [
        JiraConnector,
        ApplicationConfig,
        RefreshHistoryObserver,
        AddLogWorkObserver
      ]
    });
    TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should datesHaveTheSameYearAndMouth return equelas date', () => {
    expect(component.datesHaveTheSameYearAndMouth(new Date(), new Date())).toBeTruthy();

    const dateBefore: Date = new Date();
    dateBefore.setDate(dateBefore.getDate() - 1);
    expect(component.datesHaveTheSameYearAndMouth(new Date(), dateBefore)).toBeFalsy();
  });

  it('should removeUnwantedPartOfComment remove unneccesary text', () => {
    component.applicationConfig.removeFromComment = '(#.*#)';
    expect(component.removeUnwantedPartOfComment('test#REMOVE ME#')).toBe('test');
  });
});
