import {Component, OnInit} from '@angular/core';
import {toast} from 'angular2-materialize';
import {Observable} from 'rxjs/Rx';
import {WorkLog} from '../../service/WorkLog';
import {JiraConnector} from '../../service/JiraConnector';
import {ApplicationConfig} from '../../service/ApplicationConfig';
import {Issue} from '../../service/Issue';
import {RefreshHistoryObserver} from '../../service/RefreshHistoryObserver';
import {ApplicationConst} from '../../service/ApplicationConst';

// TODO: move to application consts
const DAY_TIME_IN_MINUTES = 8 * 60;
const TODAY_REFESH_TIME = 300 * 1000;
const EMPTY_COMMENT = 'Empty comment';
const LAST_DAY_HISTORY = 7;

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html'
})
export class HistoryComponent implements OnInit {
  history: Array<WorkLog> = [];
  progress: number;
  isFromTimer = false;
  selectedTeamMember: string;
  teamMembers: Array<String>;

  foundAnyLoggedTask: boolean;
  currentUserAvatarUrl: string;

  constructor(public jiraConnector: JiraConnector,
              public applicationConfig: ApplicationConfig,
              public refreshHistoryObserver: RefreshHistoryObserver) {
    this.teamMembers = applicationConfig.teamMembers.slice();
    this.teamMembers.push(applicationConfig.username);
    this.selectedTeamMember = applicationConfig.username;

    this.jiraConnector.setConfiguration(this.applicationConfig);
    this.generateHistoryForLastNDay(LAST_DAY_HISTORY);
    this.refreshHistoryObserver.subscribe(() => this.refresh());
  }

  public onTeamMemberChange(): void {
    console.log(`selected user: ${this.selectedTeamMember}`);
    this.history = [];
    this.generateHistoryForLastNDay(LAST_DAY_HISTORY);
  }

  public getIconColor(logProgress: number): string {
    if (logProgress >= 99) {
      return 'teal-text';
    } else if (logProgress > this.applicationConfig.minLogRatio) {
      return 'amber-text';
    } else {
      return 'red-text';
    }
  }

  public getIconName(logProgress: number): string {
    if (logProgress >= this.applicationConfig.minLogRatio) {
      return 'done';
    } else {
      return 'report_problem';
    }
  }

  public ngOnInit(): void {
    const timer = Observable.timer(TODAY_REFESH_TIME, TODAY_REFESH_TIME);
    timer.subscribe(t => {
      toast('refreshed today data', ApplicationConst.TOAST_TIME);
      this.isFromTimer = true;
      this.generateHistoryForLastNDay(LAST_DAY_HISTORY);
    });
  }

  private refresh(): any {
    this.history.length = 0;
    this.generateHistoryForLastNDay(LAST_DAY_HISTORY);
  }

  private generateHistoryForLastNDay(nDay: number): void {
    this.foundAnyLoggedTask = false;
    this.currentUserAvatarUrl = undefined;
    let count = 0;
    while (this.history.length < nDay) {
      const currentWorkLog: WorkLog = new WorkLog();

      const date: Date = new Date();
      date.setDate(date.getDate() - count);
      count++;

      if (this.isWeekend(date)) {
        continue;
      }

      currentWorkLog.date = date;

      if (this.isFromTimer) {
        this.history[0] = currentWorkLog;
      } else {
        this.history.push(currentWorkLog);
      }

      this.fillWorklogDetailForDay(date, currentWorkLog);
    }
  }

  private isWeekend(date: Date): boolean {
    const day: number = date.getDay();
    return (day === 6) || (day === 0);
  }

  private fillWorklogDetailForDay(date: Date, worklog: WorkLog): void {
    const month: number = date.getMonth() + 1;
    this.jiraConnector.getWorkLogForDay(date.getFullYear() + '-' + month + '-' + date.getDate(), this.selectedTeamMember).subscribe(
      result => {
        worklog.issues = result.issues;
        worklog.issues.forEach(issue =>
          this.jiraConnector.getIssueWorkLog(issue.key).subscribe(item => {
            this.parseIssueWorkLog(item, issue, worklog);
          })
        );
      }
    );
  }

  private parseIssueWorkLog(serviceData: any, issue: Issue, worklog: WorkLog) {
    issue.timeSpend = 0;

    serviceData.worklogs
      .filter(item => item.updateAuthor.emailAddress === this.selectedTeamMember)
      .filter(item => this.datesHaveTheSameYearAndMouth(worklog.date, new Date(item.started)))
      .forEach(item => {
        // rewrite issue data from jira worklog service to Issue object
        this.processComment(issue, item);
        issue.timeSpend += item['timeSpentSeconds'] / 60;
        this.foundAnyLoggedTask = true;

        if (!this.currentUserAvatarUrl) {
          this.currentUserAvatarUrl = item.updateAuthor.avatarUrls['48x48'];
        }

      });

    let workTimeSum = 0;
    worklog.issues.forEach(item => workTimeSum += item.timeSpend);
    worklog.totalMinutesTime = workTimeSum;
    worklog.progress = (workTimeSum / DAY_TIME_IN_MINUTES) * 100;
  }

  private processComment(issue: Issue, item: any): void {
    if (!issue.comment) {
      issue.comment = [];
    }

    const parsedComment = this.removeUnwantedPartOfComment(item['comment']);
    if (parsedComment !== '') {
      issue.comment.push(parsedComment);
    }
  }

  /**
   * Remove from comment unwanted part based on regexp from application configuration. String with only spaces are not processed.
   */
  removeUnwantedPartOfComment(comment: string): string {
    if (/^[ ]*$/.test(comment)) {
      return EMPTY_COMMENT;
    }

    if (this.applicationConfig.removeFromComment) {
      return comment.trim().replace(new RegExp(this.applicationConfig.removeFromComment), '');
    }

    return comment.trim();
  }

  datesHaveTheSameYearAndMouth(date1: Date, date2: Date): boolean {
    return date1.getDate() === date2.getDate() && date1.getFullYear() === date2.getFullYear();
  }
}
