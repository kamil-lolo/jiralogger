import {Component, OnInit} from '@angular/core';
import {JiraConnector} from '../../service/JiraConnector';
import {ApplicationConfig} from '../../service/ApplicationConfig';
import {Observable} from 'rxjs/Rx';
import {AddLogWorkEvent, AddLogWorkObserver} from '../../service/AddLogWorkObserver';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnInit {
  issues: Array<any>;
  currentIssueComments: Array<any> = null;
  currentIssueDescription: string;
  selectedIssue: string;
  isStarted = false;
  startedIssue: string;
  isPause = false;
  isReadyList = false;
  seconds = '00';
  minutes = '00';
  hours = '00';
  pauseSeconds = 0;
  pauseTimeout = false;
  isBreak = false;
  timer: Observable<number>;

  constructor(private jiraConnector: JiraConnector,
              private  applicationConfig: ApplicationConfig,
              private addLogWorkObserver: AddLogWorkObserver) {
    this.timer = Observable.timer(1000, 1000);
    this.timer.subscribe(t => this.procesTimeout());
    this.timer.subscribe(t => this.procesTimeoutPause());
  }

  startTimerOnIssue(issue: string): void {
    this.isStarted = true;
    this.startedIssue = issue;
    this.seconds = '00';
    this.minutes = '00';
    this.hours = '00';
  }

  procesTimeoutPause(): void {
    if (!this.startedIssue || !this.isPause) {
      return;
    }

    this.pauseSeconds++;
    const pauseTime = 5 * 60;
    if (this.pauseSeconds % pauseTime === 0) {
      this.pauseTimeout = true;
      new Notification('Pause is over', {
        body: 'Lets work now'
      });
    }
  }

  procesTimeout(): void {
    if (!this.startedIssue || this.isPause) {
      return;
    }

    const nextSecond = 1 + parseInt(this.seconds, 10);
    this.seconds = this.nextTimeValue(this.seconds);

    if (nextSecond === 60) {
      const nextMinut = 1 + parseInt(this.minutes, 10);

      if (nextMinut === 60) {
        this.hours = this.nextTimeValue(this.hours);
      }

      this.minutes = this.nextTimeValue(this.minutes);
    }

    this.showBreakNotification('It is pause time', 'Take 5 minutes break');
  }

  onPause(): void {
    this.isPause = !this.isPause;
    if (!this.isPause) {
      this.pauseSeconds = 0;
    }
  }

  nextTimeValue(oldValue: string): string {
    const next = 1 + parseInt(oldValue, 10);
    if (next < 10) {
      return '0' + next;
    } else if (next < 60) {
      return '' + next;
    }

    return '00';
  }

  showBreakNotification(message: string, title: string) {
    const workSessionTimeInMinutes = 30;
    const minutes = parseInt(this.minutes, 10);
    if (minutes > 0 && minutes % workSessionTimeInMinutes === 0 && parseInt(this.seconds, 10) === 0) {
      this.isBreak = true;
      new Notification(title, {
        body: message
      });
    }
  }

  showIssueDetails(issue: string): void {
    this.currentIssueComments = null;
    this.currentIssueDescription = '';

    if (this.selectedIssue === issue) { // zwiniecie aktualnego zgloszenia
      this.selectedIssue = undefined;
      return;
    }

    this.selectedIssue = issue;

    console.log(`Show details: ${issue}`);
    this.jiraConnector.getIssueDetails(issue).subscribe(
      result => {
        this.currentIssueDescription = result.fields.description || '';
        this.currentIssueDescription = this.currentIssueDescription.replace(/\n/g, '<br>');
      }
    );

    this.jiraConnector.getIssueComment(issue).subscribe(
      result => {
        this.currentIssueComments = this.sortCommentsByDate(result.comments) || [];
      }
    );
  }

  private sortCommentsByDate(comments: Array<any>): Array<any> {
    comments.sort((d1, d2) => {
      return new Date(d1.created).getTime() < new Date(d2.created).getTime() ? 1 : -1;
    });

    return comments;
  }

  stopTimerOnIssue(): void {
    this.pauseSeconds = 0;
    this.isPause = false;
    this.isStarted = false;
    this.isBreak = false;
    let workTime = '';

    if (parseInt(this.hours, 10) !== 0) {
      workTime += parseInt(this.hours, 10) + 'h ';
    }

    if (parseInt(this.minutes, 10) !== 0) {
      workTime += (parseInt(this.minutes, 10) + 1) + 'm ';
    }

    if (parseInt(this.seconds, 10) < 60 && workTime === '') {
      workTime += '1m';
    }

    const logWorkEvent = new AddLogWorkEvent(this.startedIssue, workTime);
    this.addLogWorkObserver.addLogWork(logWorkEvent);
    this.startedIssue = null;
  }

  ngOnInit(): void {
    this.jiraConnector.getAssignedIssues().subscribe(
      result => {
        this.issues = result.issues;
        this.isReadyList = true;
      }
    );
  }
}
