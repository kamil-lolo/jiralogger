import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'minutesToTime'})
export class MinutesToTime implements PipeTransform {
  transform(value: number): string {
    if (value < 60) {
      return value + 'm';
    }

    const hours = parseInt(value / 60 + '', 10);
    const minutes = value % 60;

    return hours + 'h ' + minutes + 'm';
  }
}
