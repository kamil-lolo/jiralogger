import {Component, OnInit} from '@angular/core';
import {ApplicationConfig} from '../../service/ApplicationConfig';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent {
  projectToAdd: string;
  teamMember: string;

  constructor(private  applicationConfig: ApplicationConfig) {
  }

  removeProject(project: string): void {
    this.applicationConfig.allProjects.splice(this.applicationConfig.allProjects.indexOf(project), 1);
  }

  removeTeamMember(member: string): void {
    this.applicationConfig.teamMembers.splice(this.applicationConfig.teamMembers.indexOf(member), 1);
  }

  addProject(): void {
    if (this.applicationConfig.allProjects.indexOf(this.projectToAdd) !== -1 || !this.projectToAdd) {
      return;
    }

    this.applicationConfig.allProjects.push(this.projectToAdd);
    this.applicationConfig.project = this.projectToAdd;
    this.projectToAdd = '';
  }

  addTeamMember(): void {
    if (this.applicationConfig.teamMembers.indexOf(this.teamMember) !== -1 || !this.teamMember) {
      return;
    }

    this.applicationConfig.teamMembers.push(this.teamMember);
    this.teamMember = '';
  }

  onSave(): void {
    this.applicationConfig.save();
  }
}
