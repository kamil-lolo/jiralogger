import {MdButtonModule, MdCheckboxModule, MdInputModule} from '@angular/material';
import {NgModule} from '@angular/core';
import {MdGridListModule} from '@angular/material';
import {MdDatepickerModule} from '@angular/material';
import {MdNativeDateModule} from '@angular/material';

@NgModule({
  imports: [
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdGridListModule,
    MdDatepickerModule,
    MdNativeDateModule
  ],
  exports: [
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdGridListModule,
    MdDatepickerModule,
    MdNativeDateModule
  ]
})
export class MaterialModule {

}
