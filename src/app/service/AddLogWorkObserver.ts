import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Injectable} from '@angular/core';

export class AddLogWorkEvent {
  constructor(public issue: string,
              public time: string) {

  }
}

@Injectable()
export class AddLogWorkObserver {

  public subject: BehaviorSubject<AddLogWorkEvent> = new BehaviorSubject<AddLogWorkEvent>(null);

  public addLogWork(event: AddLogWorkEvent): void {
    this.subject.next(event);
  }
}
