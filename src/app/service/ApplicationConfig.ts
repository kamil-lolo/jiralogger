import {Injectable} from '@angular/core';
import {toast} from 'angular2-materialize';
import {ApplicationConst} from './ApplicationConst';

@Injectable()
export class ApplicationConfig {
  public url: string;
  public username: string;
  public password: string;
  public minLogRatio = 80;
  public removeFromComment: string;
  public project: string;
  public configFile = 'jira-config.json';
  public visibility;
  public allProjects: Array<string> = [];
  public teamMembers: Array<string> = [];
  public rememberPassword: false;
  private hasIncorrectData = false;

  public constructor() {
    this.password = null;
    this.readConfigurationFromFile();
  }

  public save(callback = null): void {
    this.saveData(this, callback);
  }

  public saveData(data: ApplicationConfig, callback: () => void = null): void {
    fs.writeFile(this.configFile, JSON.stringify(data), err => {
      if (err) {
        toast(err);
      } else {
        if (callback) {
          callback();
          return;
        } else {
          toast('Settings saved', ApplicationConst.TOAST_TIME);
        }

        if (this.hasIncorrectData) {
          this.hasIncorrectData = false;
          window.location.reload();
        }
      }
    });
  }

  public isConfigCorrect(): boolean {
    if (!this.url || !this.username || !this.password || !this.project) {
      this.hasIncorrectData = true;
      return false;
    }

    return true;
  }

  private readConfigurationFromFile(): void {
    try {
      console.log(`fs=${fs}`);
      const unparsedConfigurationFromFile = fs.readFileSync(this.configFile, 'UTF-8');
      const configurationFromFile: any = JSON.parse(unparsedConfigurationFromFile);

      this.password = configurationFromFile.password;

      this.readField('url', configurationFromFile);
      this.readField('username', configurationFromFile);
      this.readField('project', configurationFromFile);
      this.readField('allProjects', configurationFromFile);
      this.readField('minLogRatio', configurationFromFile);
      this.readField('visibility', configurationFromFile);
      this.readField('teamMembers', configurationFromFile);
      this.readField('rememberPassword', configurationFromFile);

      if (configurationFromFile.removeFromComment) {
        this.readField('removeFromComment', configurationFromFile);
      }

      console.log(`Configuration readed`);
    } catch (e) {
      console.log(`Error while reading configuration: ${e}`);
    }
  }

  public clearPasswordIfNotRemember(): void {
    if (!this.rememberPassword && this.password) {
      const settingsCopy: ApplicationConfig = JSON.parse(JSON.stringify(this));
      settingsCopy.password = null;

      this.saveData(settingsCopy, () => {

      });
    }
  }

  private readField(name: string, configurationFromFile: any): void {
    const value = configurationFromFile[name];
    console.log(`${name}=${configurationFromFile[name]}`);
    if (value) {
      this[name] = configurationFromFile[name];
    }
  }
}
