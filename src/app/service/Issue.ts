export class Issue {
  constructor(public id: string,
              public key: string,
              public self: string,
              public summary: string,
              public comment: Array<string> = [],
              public timeSpend = 0) {
  }
}
