import {JiraConnector} from './JiraConnector';

describe('JiraConnector', () => {
  let component: JiraConnector;

  beforeEach(() => {
    component = new JiraConnector(null);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should dateToJiraFormat result date in YYYY-MM-DDThh:mm:ss.sTZD format', () => {
    const input: Date = new Date('01-01-2017 12:00');
    const result: string = JiraConnector.dateToJiraFormat(input);
    expect(result).toBe('2017-01-01T00:00:00.0+0000');
  });

});
