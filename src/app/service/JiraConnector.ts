import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {WorkLog} from './WorkLog';
import {JiraServices} from './JiraServices';
import {ApplicationConfig} from './ApplicationConfig';
import {JqlQueryBuilder} from './JqlQueryBuilder';
import {toast} from 'angular2-materialize';
import {ApplicationConst} from './ApplicationConst';
import {ServiceConnector} from './ServiceConnector';

@Injectable()
export class JiraConnector {
  private configuration: ApplicationConfig;
  private jiraUrl = '';
  private connector: ServiceConnector;

  static dateToJiraFormat(d: Date): string {
    return '' +
      d.getFullYear() + '-' +
      ('0' + (d.getMonth() + 1)).slice(-2) + '-' +
      ('0' + d.getDate()).slice(-2) + 'T00:00:00.0+0000';
  }

  constructor(private http: Http) {

  }

  public setConfiguration(configuration: ApplicationConfig): void {
    this.configuration = configuration;
    this.jiraUrl = this.configuration.url + '/jira/rest/api/2';
    this.connector = new ServiceConnector(this.configuration, this.http);
  }

  /**
   * Dla uslug POSt uzyty zostal request z modulu node, zeby uniknac problemow z CORS.
   */
  public addLogWork(issue: string, logTime: string, comment: string, date = new Date()): void {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + this.connector.getAuthorization()
    };

    const requestBody = {
      comment: comment,
      timeSpent: logTime,
      started: JiraConnector.dateToJiraFormat(date)
    };

    const options = {
      url: this.jiraUrl + JiraServices.ISSUE_WORKLOG.replace(/{issue}/g, issue),
      method: 'POST',
      headers: headers,
      json: true,
      body: requestBody
    };

    console.log(`requste to: ${options.url}`);
    request(options, function (error, response, body) {
      console.log(`Call status -> ${response.statusCode}`);

      if (!error && response.statusCode === 201) {
        toast(`Added worklog (${logTime}) in: ${issue}`, ApplicationConst.TOAST_TIME);
        console.log(body);
      } else {
        toast(`Error occurs..sorry...Check developer tools for more information`, ApplicationConst.TOAST_TIME);
        console.log(response.toJSON());
      }
    });
  }

  public getWorkLogForDay(date: string, author: string): Observable<WorkLog> {
    const query: string = new JqlQueryBuilder(this.configuration)
      .jql()
      .project(this.configuration.project)
      .worklogDate(date)
      .worklogAuthor(`\"${author}\"`)
      .build();

    return this.connector.get(this.jiraUrl + JiraServices.SEARCH + query);
  }

  public getAssignedIssues(): Observable<any> {
    const query: string = new JqlQueryBuilder(this.configuration)
      .jql()
      .project(this.configuration.project)
      .currentAuthor()
      .statusNot(['CLOSED', 'RESOLVED'])
      .build();

    return this.connector.get(this.jiraUrl + JiraServices.SEARCH + query);
  }

  public getIssueWorkLog(issue: string): Observable<any> {
    return this.connector.get(this.jiraUrl + JiraServices.ISSUE_WORKLOG.replace(/{issue}/g, issue));
  }

  public getIssueComment(issue: string): Observable<any> {
    return this.connector.get(this.jiraUrl + JiraServices.ISSUE_COMMENT.replace(/{issue}/g, issue));
  }

  public getIssueDetails(issue: string): Observable<any> {
    return this.connector.get(this.jiraUrl + JiraServices.ISSUE.replace(/{issue}/g, issue));
  }
}
