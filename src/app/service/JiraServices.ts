export namespace JiraServices {
  export const SEARCH = '/search?maxResults=25&';
  export const ISSUE_WORKLOG = '/issue/{issue}/worklog';
  export const ISSUE_COMMENT = '/issue/{issue}/comment';
  export const ISSUE = '/issue/{issue}';
}
