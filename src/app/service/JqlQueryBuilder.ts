import {ApplicationConfig} from './ApplicationConfig';

export class JqlQueryBuilder {
  query = '';
  isEmpty = true;
  link = '';

  constructor(private config: ApplicationConfig) {

  }

  private checkEmptyJql(): void {
    if (this.isEmpty) {
      this.isEmpty = false;
      this.link = ' AND ';
    }
  }

  jql(): JqlQueryBuilder {
    this.query = 'jql=';
    return this;
  }

  worklogAuthorMe(): JqlQueryBuilder {
    this.checkEmptyJql();
    this.query += 'worklogAuthor=currentUser()' + this.link;
    return this;
  }

  worklogAuthor(author: string): JqlQueryBuilder {
    this.checkEmptyJql();
    this.query += `worklogAuthor=${author}` + this.link;
    return this;
  }

  statusNot(status: Array<string>): JqlQueryBuilder {
    this.checkEmptyJql();
    this.query += `status + not + in +(${status})` + this.link;
    return this;
  }

  currentAuthor(): JqlQueryBuilder {
    this.checkEmptyJql();
    this.query += 'assignee = currentUser()' + this.link;
    return this;
  }

  worklogDate(worklogDate: string): JqlQueryBuilder {
    this.checkEmptyJql();
    this.query += 'worklogDate=' + worklogDate + this.link;
    return this;
  }

  project(projectName: string): JqlQueryBuilder {
    this.checkEmptyJql();
    this.query += 'project=' + projectName + this.link;
    return this;
  }

  build(): string {
    const withoutLastAnd = this.query.substr(0, this.query.lastIndexOf(' AND '));
    return withoutLastAnd.replace(/ /g, '+');
  }
}
