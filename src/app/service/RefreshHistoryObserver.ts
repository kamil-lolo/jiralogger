import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Injectable} from '@angular/core';

@Injectable()
export class RefreshHistoryObserver {

  public subject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public refresh(): void {
    this.subject.next(true);
  }

  public subscribe(callback: Function) {
    this.subject.subscribe(value => {
      callback(value);
    });
  }
}
