import {ApplicationConfig} from './ApplicationConfig';
import {Observable} from 'rxjs/Rx';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {Http, RequestOptions, Response, Headers} from '@angular/http';
import {toast} from 'angular2-materialize';

export class ServiceConnector {
  constructor(private configuration: ApplicationConfig, private http: Http) {

  }

  public get<T>(url: string): Observable<T> {
    const option: RequestOptions = this.getOptions();
    if (!option) {
      return Observable.empty();
    }

    return this.http
      .get(url, option)
      .map((res: Response) => res.json())
      .catch((error: any) => this.processCallError(error));
  }

  public getOptions(): RequestOptions {
    if (!this.configuration.password) {
      return null;
    }

    return new RequestOptions({headers: this.getRequestHeaders()});
  }

  public getRequestHeaders(): Headers {
    const headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('authorization', 'Basic ' + this.getAuthorization());
    return headers;
  }

  public getAuthorization(): string {
    return btoa(this.configuration.username + ':' + this.configuration.password);
  }

  public processCallError(error: any): ErrorObservable {
    if (error.status === 403) {
      if (this.configuration.password) {
        toast('Authorization error. For page reload press ctrl+r');
        this.configuration.password = null;
      }
    }

    console.log(`call service error ${error.status}`);
    return Observable.throw(error.json().error || 'Server error');
  }

}
