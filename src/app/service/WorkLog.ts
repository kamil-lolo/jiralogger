import {Issue} from './Issue';

export class WorkLog {
  constructor(public issues?: Array<Issue>,
              public date?: Date,
              public progress = 0,
              public totalMinutesTime = 0) {

  }
}
