import * as gulp from 'gulp';
import {electron} from './utils/electron';
import * as path from 'path';
import * as replace from 'gulp-replace';
import * as typescript from 'gulp-tsc';
import * as gulpUtil from 'gulp-util';
/**
 * Restarts the current electron window instance with the changes
 */
gulp.task('build.restart', electron.restart);

/**
 * The default build task - sets up the underlying watch task
 */
gulp.task('build.default', ['build.watch', 'build.npm.copy.img']);

/**
 * Starts the electron listener for livereload
 * Sets up a listener on the AngularCLI dist directory for changes
 */
gulp.task('build.watch', () => {
  electron.start();

  gulp.watch(path.join(process.cwd(), '/src/**/*.*')).on('change', file => {
    gulpUtil.log(gulpUtil.colors.yellow('file changed' + ' (' + file.path + ')'));
  });
});

/**
 * Copies the images to dist.s
 */
gulp.task('build.npm.copy.img', () => {
  return gulp.src(path.join(process.cwd(), '/src/**/*.svg'))
    .pipe(gulp.dest(path.join(process.cwd(), '/dist/')));
});

/**
 * Copies the package.json required for Electron to create package/bundles
 */
gulp.task('build.npm.copy', () => {
  return gulp.src(path.join(process.cwd(), 'package.json'))
    .pipe(gulp.dest(path.join(process.cwd(), '/dist/')));
});

/**
 * Copies the package.json required for Electron to create package/bundles
 */
gulp.task('bundle.clean', () => {
  console.log(path.join(process.cwd(), 'dist', 'electron/electron-app.js'));
  return gulp.src(path.join(process.cwd(), 'dist', 'electron/electron-app.js'))
    .pipe(replace('var electron_connect_1 = require("electron-connect");', '', {logs: {enabled: true}}))
    .pipe(replace('electron_connect_1.client.create(applicationRef);', '', {logs: {enabled: true}}))
    .pipe(gulp.dest(path.join(process.cwd(), '/dist')));
});

/**
 * Compile ts main electron script.
 */
gulp.task('build.electron', () => {
  return gulp.src(['src/electron/**/*.ts'])
    .pipe(typescript())
    .pipe(gulp.dest('dist/electron'));
});
